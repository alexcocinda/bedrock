#!/bin/bash

current_directory=$(pwd)

optstring=":hS"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -S      system install\n"
}

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    S)
      system_install=true
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

function install_alacritty() {
  if [[ "$system_install" == "true" ]]; then
    sudo add-apt-repository ppa:aslatter/ppa
    sudo apt update && sudo apt install -y alacritty
  fi
  ln -sfn ${current_directory}/alacritty ~/.config/alacritty
}

function install_tmux() {
  if [[ "$system_install" == "true" ]]; then
    sudo apt install -y tmux
  fi
  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
  ln -sf ${current_directory}/tmux/tmux.conf ~/.tmux.conf
}

function install_zsh() {
  if [[ "$system_install" == "true" ]]; then
    sudo apt install -y zsh
    echo 'export ZDOTDIR="/home/${USER}/.config/bedrock/zsh"' | sudo tee -a /etc/zsh/zshenv
  fi
  sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  git clone https://github.com/sindresorhus/pure.git "$HOME/.zsh/pure"
  ln -sfn ${current_directory}/zsh/scripts ~/.local/bin/scripts
}

function install_getnf() {
  curl -fsSL https://raw.githubusercontent.com/ronniedroid/getnf/master/install.sh | sh
}

function install_neovim() {

  if [[ "$system_install" == "true" ]]; then
    wget -P /tmp https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
    chmod +x /tmp/nvim.appimage
    sudo mv /tmp/nvim.appimage /usr/local/bin/nvim
    sudo npm install -g prettier tree-sitter neovim
    sudo apt install -y fd-find luarocks
  fi

  pip3 install --user --upgrade \
    neovim \
    neovim-remote \
    pynvim \
    black \
    Pillow

  cargo install stylua tree-sitter-cli
  ln -sfn ${current_directory}/nvim ~/.config/nvim
}

function install_vscode() {

  ln -sfn ${current_directory}/Code ~/.config/Code

  if [[ "$system_install" == "true" ]]; then
    sudo apt install -y software-properties-common apt-transport-https
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
    sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
    sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
    sudo apt update && sudo apt install -y code
  fi

  extensions=(
    "asvetliakov.vscode-neovim"
    "jdinhlife.gruvbox"
    "navernoedenis.gruvbox-material-icons"
    "ms-vscode.cpptools-extension-pack"
    "llvm-vs-code-extensions.vscode-clangd"
    "ms-azuretools.vscode-docker"
  )

  for extension in ${extensions[@]}
  do
    code --install-extension ${extension}
  done
}

function install_fzf() {
  git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
  ~/.fzf/install
}

function install_ranger() {
  if [[ "$system_install" == "true" ]]; then
    sudo apt install -y ranger
  fi
  git clone https://github.com/alexanderjeurissen/ranger_devicons ~/.config/ranger/plugins/ranger_devicons
  ln -sf ${current_directory}/ranger/rc.conf ~/.config/ranger/rc.conf
}

function install_vifm() {

  if [[ "$system_install" == "true" ]]; then
    sudo apt install -y vifm
  fi
  git clone https://github.com/cirala/vifmimg.git ~/.config/vifm/plugins/vifmimg
  ln -sf ${current_directory}/vifm/vifmrc ~/.config/vifm/vifmrc
  ln -sf ${current_directory}/vifm/icons.vifm ~/.config/vifm/icons.vifm
  ln -sfn ${current_directory}/vifm/colors ~/.config/vifm/colors
  ln -sfn ${current_directory}/vifm/colors ~/.config/vifm/scripts
}

function install_neofetch() {
  if [[ "$system_install" == "true" ]]; then
    sudo apt install -y neofetch
  fi
  ln -sfn ${current_directory}/neofetch ~/.config/neofetch
}

function install_node() {
  if [[ "$system_install" == "true" ]]; then
    sudo apt update && sudo apt install -y curl
    curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
    sudo apt update && sudo apt install -y nodejs yarn
  fi
}

function install_rust() {
  curl https://sh.rustup.rs -sSf | sh
}

function install_packages() {

  if [[ "$system_install" == "true" ]]; then
    sudo apt update && sudo apt install -y \
      git \
      python3-pip \
      python3-venv \
      ripgrep \
      libjpeg8-dev \
      zlib1g-dev \
      python3-dev \
      libxtst-dev \
      clangd-12 libstdc++-12-dev \
      imagemagick \
      xdotool \
      sxiv \
      zathura \
      golang
  fi

  install_rust
  install_node
  install_getnf
  install_alacritty
  install_tmux
  install_zsh
  install_neovim
  install_vscode
  install_fzf
  install_ranger
  install_vifm
  install_neofetch
}

function install_defaults() {
  mkdir -p ~/.local/man/man1
  mkdir -p ~/.local/completion
  mkdir -p ~/.local/share/applications
  for file in ${current_directory}/defaults/*
  do
    ln -sf ${file} ~/.local/share/applications/$(basename ${file})
  done
  sudo update-alternatives --install /usr/bin/clangd clangd /usr/bin/clangd-12 100
}

function install_services() {
  for service in ${current_directory}/services/*
  do
    sudo ln -sf ${service} /etc/systemd/system
    sudo systemctl enable --now $(basename ${service})
  done
}

install_packages
install_defaults
#install_services
