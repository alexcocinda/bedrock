# Bedrock

This can serve as the foundation for any working environment in a Debian based distro.

Environment variables should be stored in `~/.zshenv`.

### Nerd Fonts

Use `getnf` or install all Nerd Fonts.

1. Clone the [nerd-fonts](https://github.com/ryanoasis/nerd-fonts.git) repo.

```zsh
git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git
```

2. Execute the install script.

```zsh
cd nerd-fonts
./install.sh
```

3. Select one of the installed fonts from the terminal's settings.

### Install

1. Clone the repository.

```zsh
git clone https://gitlab.com/alexcocinda/bedrock.git ~/.config/bedrock
```

2. Execute the install script to install packages and create symbolic links.

```zsh
./install.sh
```

### Completion

Save any completion scripts in the `zsh/completion` folder. The contents will be sourced by the `.zshrc` file.

### Terminfo

1. Check if the `terminfo` config is present.

```zsh
infocmp alacritty
```

You should get a similar output:

```zsh
#       Reconstructed via infocmp from file: /etc/terminfo/a/alacritty
alacritty|alacritty terminal emulator,
        am, bce, ccc, hs, km, mc5i, mir, msgr, npc, xenl,
        colors#0x100, cols#80, it#8, lines#24, pairs#0x7fff,
...
```

Otherwise, follow the post build instructions from [here](https://github.com/alacritty/alacritty/blob/master/INSTALL.md#desktop-entry).

### Bluetooth

If Bluetooth does not work, refer to the [official guide](https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Bluetooth/#index5h2) or [this SO thread](https://askubuntu.com/questions/831331/failed-to-change-profile-to-headset-head-unit). The service file from this repo can be used to automate the process, but the binaries location has to be configured.

### Recommended software

- [CMake](https://cmake.org/download)
- [picocom](https://github.com/npat-efault/picocom)
- [clangd](https://clangd.llvm.org)
- [fzf](https://github.com/junegunn/fzf)
- [fontpreview](https://github.com/sdushantha/fontpreview)
- [epub-thumbnailer](https://github.com/marianosimone/epub-thumbnailer)

### VirtualBox

When it's unavoidable, virtual machines are necessary. By default, full screen is not enabled, so follow the next instructions to gain full screen support. These steps are extracted from [here](https://askubuntu.com/questions/22743/how-do-i-install-guest-additions-in-a-virtualbox-vm).

1. Install dependencies.

```zsh
sudo apt-get update
sudo apt-get install build-essential linux-headers-$(uname -r)
sudo apt-get install virtualbox-guest-x11
```

2. Install Guest Additions from VirtualBox by going to `Devices` -> `Insert Guest Additions CD Image...` and follow the install instructions.

### Credits

All of this was possible due to the tutorials and blogs of the following people:

- Ben Awad: [YouTube channel](https://www.youtube.com/channel/UC-8QAzbLcRglXeN_MY9blyw)
- ChrisAtMachine: [YouTube channel](https://www.youtube.com/channel/UCS97tchJDq17Qms3cux8wcA) and [blog](https://www.chrisatmachine.com/)
- Brodie Robertson: [YouTube channel](https://www.youtube.com/channel/UCld68syR8Wi-GY_n4CaoJGA)

### Amazing resources

Vim:

- https://vimawesome.com/
- https://vim-adventures.com/
- https://www.barbarianmeetscoding.com/blog/boost-your-coding-fu-with-vscode-and-vim
- https://medium.com/predict/using-neovim-as-a-window-manager-ef7f559b3008
- https://github.com/nanotee/nvim-lua-guide
- https://ianding.io/2019/07/29/configure-coc-nvim-for-c-c++-development/
- https://vim.fandom.com/wiki/Avoid_the_escape_key
- https://www.tecmint.com/vi-editor-usage/
- http://vimsheet.com/
- https://www.fprintf.net/vimCheatSheet.html
- https://stackoverflow.com/questions/235839/indent-multiple-lines-quickly-in-vi
- https://stackoverflow.com/questions/3458689/how-to-move-screen-without-moving-cursor-in-vim
- https://vim.fandom.com/wiki/Search_and_replace_in_a_visual_selection

Tmux:

- https://github.com/rothgar/awesome-tmux

Zsh:

- https://scriptingosx.com/2019/06/moving-to-zsh/
- https://jdhao.github.io/2019/06/13/zsh_bind_keys/

Vifm:

- https://vifm.info/cheatsheets.shtml
