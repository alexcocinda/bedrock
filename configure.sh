#!/bin/bash

current_directory=$(pwd)

function configure_alacritty() {
  ln -sfn ${current_directory}/alacritty ~/.config/alacritty
}

function configure_tmux() {
  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
  ln -sf ${current_directory}/tmux/tmux.conf ~/.tmux.conf
}

function configure_zsh() {
  sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  git clone https://github.com/sindresorhus/pure.git "$HOME/.zsh/pure"
  echo 'export ZDOTDIR="${HOME}/.config/bedrock/zsh"' | sudo tee -a /etc/zsh/zshenv
}

function configure_neovim() {
  ln -sfn ${current_directory}/nvim ~/.config/nvim
}

function configure_fzf() {
  git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
  ~/.fzf/install
}

function configure_ranger() {
  git clone https://github.com/alexanderjeurissen/ranger_devicons ~/.config/ranger/plugins/ranger_devicons
  ln -sf ${current_directory}/ranger/rc.conf ~/.config/ranger/rc.conf
}

function configure_vifm() {
  git clone https://github.com/cirala/vifmimg.git ~/.config/vifm/plugins/vifmimg
  ln -sf ${current_directory}/vifm/vifmrc ~/.config/vifm/vifmrc
  ln -sf ${current_directory}/vifm/icons.vifm ~/.config/vifm/icons.vifm
  ln -sfn ${current_directory}/vifm/colors ~/.config/vifm/colors
  ln -sfn ${current_directory}/vifm/colors ~/.config/vifm/scripts
}

function configure_neofetch() {
  ln -sfn ${current_directory}/neofetch ~/.config/neofetch
}

function configure_packages() {

  configure_node
  configure_alacritty
  configure_tmux
  configure_zsh
  configure_neovim
  configure_lazygit
  configure_fzf
  configure_ranger
  configure_vifm
  configure_neofetch
}

function configure_defaults() {
  mkdir -p ~/.local/man/man1
  mkdir -p ~/.local/completion
  mkdir -p ~/.local/share/applications
  for file in ${current_directory}/defaults/*
  do
    ln -sf ${file} ~/.local/share/applications/$(basename ${file})
  done
}

function configure_services() {
  for service in ${current_directory}/services/*
  do
    sudo ln -sf ${service} /etc/systemd/system
    sudo systemctl enable --now $(basename ${service})
  done
}

configure_defaults
configure_packages
