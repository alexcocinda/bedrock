return {
  "rmagatti/auto-session",
  lazy = false,
  opts = {
    auto_restore_enabled = true,
    auto_session_suppress_dirs = { "~/", "~/Downloads", "~/Documents", "~/Desktop/", "/workspace/" },
  }
}
