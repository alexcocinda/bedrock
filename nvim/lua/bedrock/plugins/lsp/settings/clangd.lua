local capabilities = vim.lsp.protocol.make_client_capabilities()

return {
	capabilities = vim.tbl_deep_extend("force", capabilities, {
		-- Force offset encoding to utf-16 because copilot is limited to utf-16
		offsetEncoding = { "utf-16" },
		general = {
			positionEncodings = { "utf-16" },
		},
	}),
}
