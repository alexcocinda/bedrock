return {
  "sentriz/vim-print-debug",
  config = function()
    vim.cmd([[ let g:print_debug_templates = {
    \   'go':         'fmt.Printf("+++ {}\n")',
    \   'python':     'print(f"+++ {}")',
    \   'javascript': 'console.log(`+++ {}`);',
    \   'c':          'printf("+++ {}\n");',
    \   'cpp':        'printf("+++ {}\n");',
    \ }
    ]])
  end,
}
