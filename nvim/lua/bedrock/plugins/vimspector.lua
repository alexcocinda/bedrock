return {
  "puremourning/vimspector",
  config = function()
    vim.g.vimspector_base_dir = os.getenv("HOME") .. "/.config/nvim/vimspector-config"
  end,
}
