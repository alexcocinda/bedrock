return {
  "gbprod/substitute.nvim",
  event = { "BufReadPre", "BufNewFile" },
  config = function()
    local substitute = require("substitute")

    substitute.setup()

    -- set keymaps
    local map_key = vim.keymap.set

    map_key("n", "s", substitute.operator, { desc = "Substitute with motion" })
    map_key("n", "ss", substitute.line, { desc = "Substitute line" })
    map_key("n", "S", substitute.eol, { desc = "Substitute to end of line" })
    map_key("x", "s", substitute.visual, { desc = "Substitute in visual mode" })
  end,
}
