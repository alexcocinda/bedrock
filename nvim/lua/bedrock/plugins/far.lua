return {
  "brooth/far.vim",
  config = function()
    vim.cmd("let g:far#source='rg'")

    vim.cmd("let g:far#window_width=50")
    vim.cmd("let g:far#file_mask_favorites=['%:p', '**/*.*', '**/*.cxx', '**/*.hxx', '**/*.c', '**/*.h', '**/*.py', '**/*.vim', '**/*.lua' ]")
    vim.cmd("let g:far#window_min_content_width=30")
    vim.cmd("let g:far#enable_undo=1")
  end,
}
