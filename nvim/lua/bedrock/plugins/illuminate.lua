return {
  "RRethy/vim-illuminate",
  config = function()
    local status_ok, illuminate = pcall(require, "illuminate")
    if not status_ok then
      return
    end

    vim.g.Illuminate_ftblacklist = { "alpha", "NvimTree" }

    local map_key = vim.keymap.set

    map_key(
      "n",
      "<a-n>",
      '<cmd>lua require"illuminate".next_reference{wrap=true}<cr>',
      { noremap = true }
    )
    map_key(
      "n",
      "<a-p>",
      '<cmd>lua require"illuminate".next_reference{reverse=true,wrap=true}<cr>',
      { noremap = true }
    )

    illuminate.configure({
      providers = {
        "lsp",
        "treesitter",
        "regex",
      },
      delay = 200,
      filetypes_denylist = {
        "dirvish",
        "fugitive",
        "alpha",
        "NvimTree",
        "packer",
        "neogitstatus",
        "Trouble",
        "lir",
        "Outline",
        "spectre_panel",
        "toggleterm",
        "DressingSelect",
        "TelescopePrompt",
      },
      filetypes_allowlist = {},
      modes_denylist = {},
      modes_allowlist = {},
      providers_regex_syntax_denylist = {},
      providers_regex_syntax_allowlist = {},
      under_cursor = true,
    })

    vim.cmd([[hi def IlluminatedWordText gui=bold,italic]])
    vim.cmd([[hi def IlluminatedWordRead gui=bold,italic]])
    vim.cmd([[hi def IlluminatedWordWrite gui=bold,italic,underline]])
  end,
}
