return {
  "vifm/vifm.vim",
  config = function()
    vim.g.vifm_replace_netrw = 1
    vim.g.vifm_replace_netrw_cmd = "Vifm"
    vim.g.vifm_embed_split = 1
  end,
}
