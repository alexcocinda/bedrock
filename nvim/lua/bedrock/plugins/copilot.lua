return {
  "github/copilot.vim",
  config = function()
    -- use this table to disable/enable filetypes
    vim.g.copilot_filetypes = { markdown = true }
    -- vim.g.copilot_filetypes = { ["*"] = false, python = true }

    vim.g.copilot_no_tab_map = true

    local map_key = vim.keymap.set

    map_key("i", "<C-a>", 'copilot#Accept("<CR>")', {
      expr = true,
      replace_keycodes = false,
    })

    map_key("i", "<C-D>", "<Plug>(copilot-dismiss)")
    map_key("i", "<C-J>", "<Plug>(copilot-next)")
    map_key("i", "<C-K>", "<Plug>(copilot-previous)")
    map_key("i", "<C-S>", "<Plug>(copilot-suggest)")
    map_key("i", "<C-W>", "<Plug>(copilot-accept-word)")
    map_key("i", "<C-L>", "<Plug>(copilot-accept-line)")

    vim.cmd([[highlight CopilotSuggestion guifg=#555555 ctermfg=8]])
  end,
}
