return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  init = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 500
  end,
  opts = {
    win = {
      padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
      winblend = 0,
    },
    plugins = {
      marks = true,       -- shows a list of your marks on ' and `
      registers = true,   -- shows your registers on " in NORMAL or <C-r> in INSERT mode
      spelling = {
        enabled = true,   -- enabling this will show WhichKey when pressing z= to select spelling suggestions
        suggestions = 20, -- how many suggestions should be shown in the list?
      },
      -- the presets plugin, adds help for a bunch of default keybindings in Neovim
      -- No actual key bindings are created
      presets = {
        operators = false,   -- adds help for operators like d, y, ... and registers them for motion / text object completion
        motions = true,      -- adds help for motions
        text_objects = true, -- help for text objects triggered after entering an operator
        windows = true,      -- default bindings on <c-w>
        nav = true,          -- misc bindings to work with windows
        z = true,            -- bindings for folds, spelling and others prefixed with z
        g = true,            -- bindings for prefixed with g
      },
    },
    -- add operators that will trigger motion and text object completion
    -- to enable all native operators, set the preset / operators plugin above
    -- operators = { gc = "Comments" },
    icons = {
      breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
      separator = "➜", -- symbol used between a key and it's label
      group = "+", -- symbol prepended to a group
    },
    layout = {
      height = { min = 4, max = 25 }, -- min and max height of the columns
      width = { min = 20, max = 50 }, -- min and max width of the columns
      spacing = 3,                    -- spacing between columns
      align = "left",                 -- align columns left, center or right
    },
    show_help = true,                 -- show help message on the command line when the popup is visible
    -- triggers = {"<leader>"} -- or specify a list manually
  },
  config = function()
    local status_ok, which_key = pcall(require, "which-key")
    if not status_ok then
      return
    end

    local normal_opts = {
      mode = "n",     -- NORMAL mode
      prefix = "<leader>",
      buffer = nil,   -- Global mappings. Specify a buffer number for buffer local mappings
      silent = true,  -- use `silent` when creating keymaps
      noremap = true, -- use `noremap` when creating keymaps
      nowait = true,  -- use `nowait` when creating keymaps
    }

    local visual_opts = {
      mode = "v",     -- NORMAL mode
      prefix = "<leader>",
      buffer = nil,   -- Global mappings. Specify a buffer number for buffer local mappings
      silent = true,  -- use `silent` when creating keymaps
      noremap = true, -- use `noremap` when creating keymaps
      nowait = true,  -- use `nowait` when creating keymaps
    }

    local mappings = {
      { "<leader>;", "A;<Esc>",        desc = "End in ;" },
      { "<leader>,", "A,<Esc>",        desc = "End in ," },
      { "<leader>.", "A.<Esc>",        desc = "End in ." },
      { "<leader>a", "<cmd>Alpha<cr>", desc = "Alpha" },
      { "<leader>D",     ":Bdelete<CR>",                                                     desc = "Close Buffer" },
      { "<leader><Del>", "<cmd>Bdelete!<CR>",                                                desc = "Close Buffer" },
      { "<leader>e",     "<cmd>NvimTreeToggle<cr>",                                          desc = "Explorer" },
      { "<leader>h",     "<C-W>s",                                                           desc = "Horizontal split" },
      { "<leader>L",     "<C-^>",                                                            desc = "Last buffer" },
      { "<leader>n",     "<cmd>nohlsearch<CR>",                                              desc = "No Highlight" },
      { "<leader>P",     "<cmd>lua require('telescope').extensions.projects.projects()<cr>", desc = "Projects" },
      { "<leader>q",     ":q<CR>",                                                           desc = "Quit" },
      { "<leader>Q",     "<cmd>wqa<CR>",                                                     desc = "Quit" },
      { "<leader>S",     "<cmd>SwitchSourceHeader<CR>",                                      desc = "S/H" },
      { "<leader>u",     "<cmd>UndotreeToggle<CR>",                                          desc = "Save" },
      { "<leader>v",     "<C-W>v",                                                           desc = "Vertical split" },
      { "<leader>=",     "<C-W>=",                                                           desc = "Equalize" },
      { "<leader>w",     "<cmd>w<CR>",                                                       desc = "Save" },
      { "<leader>W",     "<cmd>wa<CR>",                                                      desc = "Save" },
      { "<leader>T",     "<cmd>Telescope live_grep theme=ivy<cr>",                           desc = "Find Text" },

      { "<leader>c",     group = "Comment" },
      { "<leader>cd",    "<cmd>Dox<cr>",                                                     desc = "Dox" },
      { "<leader>cc",    "<Plug>(comment_toggle_linewise_current)",                                       desc = "Comment" },
      { "<leader>cb",    "<Plug>(comment_toggle_blockwise_current)",                                       desc = "Comment" },

      { "<leader>d",     group = "Debug" },
      { "<leader>da",    "<cmd>call print_debug#print_debug()<cr>",                          desc = "add debug statement" },
      { "<leader>db",    "<Plug>VimspectorToggleBreakpoint",                                 desc = "breakpoint" },
      { "<leader>dB",    "<Plug>VimspectorToggleConditionalBreakpoint",                      desc = "conditional breakpoint" },
      { "<leader>dc",    "<Plug>VimspectorRunToCursor",                                      desc = "run to cursor" },
      { "<leader>dd",    "<Plug>VimspectorContinue",                                         desc = "continue" },
      { "<leader>df",    "<Plug>VimspectorAddFunctionBreakpoint",                            desc = "function breakpoint" },
      { "<leader>dm",    "<cmd>MaximizerToggle<CR>",                                         desc = "maximize window" },
      { "<leader>do",    "<Plug>VimspectorStepOver",                                         desc = "step over" },
      { "<leader>dO",    "<Plug>VimspectorStepOut",                                          desc = "step out" },
      { "<leader>di",    "<Plug>VimspectorStepInto",                                         desc = "step into" },
      { "<leader>dp",    "<Plug>VimspectorPause",                                            desc = "pause" },
      { "<leader>dr",    "<Plug>VimspectorRestart",                                          desc = "restart" },
      { "<leader>ds",    "<Plug>VimspectorStop",                                             desc = "stop" },

      { "<leader>f",     group = "Files" },
      { "<leader>fe",    "<cmd>NvimTreeToggle<CR>",                                          desc = "Toggle file explorer" },
      {
        "<leader>ff",
        "<cmd>lua require('telescope.builtin').find_files(require('telescope.themes').get_dropdown{previewer = false})<cr>",
        desc = "Find files",
      },

      { "<leader>g",   group = "Git" },
      { "<leader>gg",  "<cmd>LazyGit<CR>",                                   desc = "Lazygit" },
      { "<leader>gj",  "<cmd>lua require 'gitsigns'.next_hunk()<cr>",        desc = "Next Hunk" },
      { "<leader>gk",  "<cmd>lua require 'gitsigns'.prev_hunk()<cr>",        desc = "Prev Hunk" },
      { "<leader>gl",  "<cmd>lua require 'gitsigns'.blame_line()<cr>",       desc = "Blame" },
      { "<leader>gp",  "<cmd>lua require 'gitsigns'.preview_hunk()<cr>",     desc = "Preview Hunk" },
      { "<leader>gr",  "<cmd>lua require 'gitsigns'.reset_hunk()<cr>",       desc = "Reset Hunk" },
      { "<leader>gR",  "<cmd>lua require 'gitsigns'.reset_buffer()<cr>",     desc = "Reset Buffer" },
      { "<leader>gs",  "<cmd>lua require 'gitsigns'.stage_hunk()<cr>",       desc = "Stage Hunk" },
      { "<leader>gu",  "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>",  desc = "Undo Stage Hunk" },
      { "<leader>go",  "<cmd>Telescope git_status<cr>",                      desc = "Open changed file" },
      { "<leader>gb",  "<cmd>Telescope git_branches<cr>",                    desc = "Checkout branch" },
      { "<leader>gc",  "<cmd>Telescope git_commits<cr>",                     desc = "Checkout commit" },
      { "<leader>gd",  "<cmd>Gitsigns diffthis HEAD<cr>",                    desc = "Diff" },

      { "<leader>k",   group = "Tasks" },
      { "<leader>kc",  "<cmd>AsyncTask configure<CR>",                       desc = "configure" },
      { "<leader>kb",  "<cmd>AsyncTask build<CR>",                           desc = "build" },
      { "<leader>kf",  "<cmd>AsyncTask build-file<CR>",                      desc = "build" },
      { "<leader>kt",  "<cmd>AsyncTask test<CR>",                            desc = "run tests" },
      { "<leader>kr",  "<cmd>AsyncTask run<CR>",                             desc = "run" },
      { "<leader>ki",  "<cmd>AsyncTask install<CR>",                         desc = "install" },
      { "<leader>kC",  "<cmd>AsyncTask clean<CR>",                           desc = "clean" },
      { "<leader>kd",  "<cmd>AsyncTask delete<CR>",                          desc = "delete" },
      { "<leader>ke",  "<cmd>AsyncTaskEdit<CR>",                             desc = "edit local tasks" },
      { "<leader>kg",  "<cmd>AsyncTaskEdit!<CR>",                            desc = "edit global tasks" },
      { "<leader>kh",  "<cmd>AsyncTaskList!<CR>",                            desc = "list hidden tasks" },
      { "<leader>kl",  "<cmd>CocList tasks<CR>",                             desc = "list tasks" },

      { "<leader>kR",  group = "ROS" },
      { "<leader>kRb", "<cmd>AsyncTask ros-build-current<CR>",               desc = "build current" },
      { "<leader>kRB", "<cmd>AsyncTask ros-build<CR>",                       desc = "build" },
      { "<leader>kRr", "<cmd>AsyncTask ros-run-current<CR>",                 desc = "run current" },
      { "<leader>kRR", "<cmd>AsyncTask ros-run<CR>",                         desc = "run" },

      { "<leader>l",   group = "LSP" },
      { "<leader>la",  vim.lsp.buf.code_action,                              desc = "Code Action" },
      { "<leader>lb",  "<cmd>Telescope diagnostics bufnr=0<CR>",             desc = "CodeLens Action" },
      { "<leader>lc",  "<cmd>lua vim.lsp.codelens.run()<cr>",                desc = "CodeLens Action" },
      { "<leader>ld",  vim.lsp.buf.declaration,                              desc = "Document Diagnostics" },
      { "<leader>lf",  "<cmd>lua vim.lsp.buf.format{async=true}<cr>",        desc = "Format" },
      { "<leader>li",  "<cmd>Telescope lsp_implementations<cr>",             desc = "Format" },
      { "<leader>lj",  vim.diagnostic.goto_next,                             desc = "Next Diagnostic" },
      { "<leader>lk",  vim.diagnostic.goto_prev,                             desc = "Prev Diagnostic" },
      { "<leader>ll",  vim.diagnostic.open_float,                            desc = "LocationList" },
      { "<leader>lm",  "<cmd>Mason<cr>",                                     desc = "Mason Info" },
      { "<leader>lq",  "<cmd>lua vim.lsp.buf.code_action({apply=true})<cr>", desc = "QuickFix" },
      { "<leader>lr",  "<cmd>Telescope lsp_references<cr>",                  desc = "References" },
      { "<leader>ls",  "<cmd>Telescope lsp_document_symbols<cr>",            desc = "Document Symbols" },
      { "<leader>lt",  "<cmd>Telescope lsp_type_definitions<cr>",            desc = "Definitions" },
      { "<leader>lI",  "<cmd>LspInfo<cr>",                                   desc = "Info" },
      { "<leader>lD",  "<cmd>Telescope lsp_definitions<cr>",                 desc = "Definitions" },
      { "<leader>lR",  vim.lsp.buf.rename,                                   desc = "Rename" },
      { "<leader>lS",  "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>",   desc = "Workspace Symbols" },

      { "<leader>r",   group = "Replace" },
      { "<leader>rb",  "<cmd>Farr --source=vimgrep<CR>",                     desc = "replace in buffer" },
      { "<leader>rB",  "<cmd>Farp --source=vimgrep<CR>",                     desc = "delete in buffer" },
      { "<leader>rp",  "<cmd>Farr --source=rg<CR>",                          desc = "replace in project" },
      { "<leader>rP",  "<cmd>Farp --source=rg<CR>",                          desc = "delete in project" },
      { "<leader>ru",  "<cmd>Farundo<CR>",                                   desc = "undo" },

      { "<leader>s",   group = "Search" },
      {
        "<leader>sb",
        "<cmd>lua require('telescope.builtin').buffers(require('telescope.themes').get_dropdown{previewer = false})<cr>",
        desc = "Buffers",
      },
      { "<leader>sf",  "<cmd>Telescope find_files<cr>",                      desc = "Find files" },
      { "<leader>sg",  "<cmd>Telescope live_grep<cr>",                       desc = "Find Text" },
      { "<leader>sh",  "<cmd>Telescope help_tags<cr>",                       desc = "Find Help" },
      { "<leader>sr",  "<cmd>Telescope oldfiles<cr>",                        desc = "Open Recent File" },
      { "<leader>st",  "<cmd>TodoTelescope<cr>",                             desc = "Open Recent File" },
      { "<leader>sw",  "<cmd>Telescope grep_string<cr>",                     desc = "Find word under cursor" },
      { "<leader>sk",  "<cmd>Telescope keymaps<cr>",                         desc = "Keymaps" },
      { "<leader>sB",  "<cmd>Telescope git_branches<cr>",                    desc = "Checkout branch" },
      { "<leader>sC",  "<cmd>Telescope commands<cr>",                        desc = "Commands" },
      { "<leader>sM",  "<cmd>Telescope man_pages<cr>",                       desc = "Man Pages" },
      { "<leader>sR",  "<cmd>Telescope registers<cr>",                       desc = "Registers" },

      { "<leader>t",   group = "Terminal" },
      { "<leader>tp",  "<cmd>lua _PYTHON_TOGGLE()<cr>",                      desc = "Python" },
      { "<leader>tf",  "<cmd>ToggleTerm direction=float<cr>",                desc = "Float" },
      { "<leader>th",  "<cmd>ToggleTerm size=10 direction=horizontal<cr>",   desc = "Horizontal" },
      { "<leader>tv",  "<cmd>ToggleTerm size=80 direction=vertical<cr>",     desc = "Vertical" },
    }
    which_key.add(mappings, normal_opts)
    which_key.add(mappings, visual_opts)
  end,
}
