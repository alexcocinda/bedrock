return {
  "ellisonleao/gruvbox.nvim",
  priority = 1000,
  config = function()
    local soft_bg = "#282828"
    local hard_bg = "#1d2021"
    local contrast = "hard"

    local gutter_bg
    if contrast == "hard" then
      gutter_bg = hard_bg
    else
      gutter_bg = soft_bg
    end

    require("gruvbox").setup({
      terminal_colors = true, -- add neovim terminal colors
      undercurl = true,
      underline = true,
      bold = true,
      italic = {
        strings = true,
        emphasis = true,
        comments = true,
        operators = false,
        folds = true,
      },
      strikethrough = true,
      invert_selection = false,
      invert_signs = false,
      invert_tabline = false,
      invert_intend_guides = false,
      inverse = true,      -- invert background for search, diffs, statuslines and errors
      contrast = contrast, -- can be "hard", "soft" or empty string
      palette_overrides = {},
      dim_inactive = false,
      transparent_mode = false,
      overrides = {
        SignColumn = { bg = gutter_bg },
      }
    })

    vim.cmd("colorscheme gruvbox")

    vim.api.nvim_set_hl(0, "DiagnosticSignError", { bg = gutter_bg, fg = "#fb4934" })
    vim.api.nvim_set_hl(0, "DiagnosticSignWarn", { bg = gutter_bg, fg = "#fabd2f" })
    vim.api.nvim_set_hl(0, "DiagnosticSignInfo", { bg = gutter_bg, fg = "#83a598" })
    vim.api.nvim_set_hl(0, "DiagnosticSignHint", { bg = gutter_bg, fg = "#8ec07c" })

    -- Hide all semantic highlights
    vim.api.nvim_set_hl(0, '@lsp.type.function', {})
    for _, group in ipairs(vim.fn.getcompletion("@lsp", "highlight")) do
      vim.api.nvim_set_hl(0, group, {})
    end

    -- Underscores are highlighted in floating windows for some reason.
    vim.cmd("autocmd BufWinEnter *.cxx,*.hxx,*.cpp,*.hpp,*.c,*.h hi clear Error")
  end,
}
