# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'jimeh/tmux-themepack'
set -g @plugin 'christoomey/vim-tmux-navigator'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-open'
set -g @plugin 'tmux-plugins/tmux-sidebar'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'

# Theme
set -g @themepack 'basic'
# 256 colors
# set -g default-terminal "tmux-256color"
# Truecolor
set -g default-terminal "alacritty"
set -ga terminal-overrides ",alacritty:RGB"

# Custom
set-window-option -g mode-keys vi
set -g set-clipboard on
set -g allow-rename off

# Search engines
set -g @open-D 'https://www.duckduckgo.com/?q='

# Key bindings
# This is mapped in zsh, but it's a good example for running commands
# inside the current pane.
# bind C-c send-keys -t $TMUX_PANE 'clear' Enter
bind C-q kill-server

# Git
set -g focus-events on

# Restore tmux environment
set -g @resurrect-strategy-nvim 'session'
set -g @resurrect-capture-pane-contents 'off'
set -g @continuum-restore 'on'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
