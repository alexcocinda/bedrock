function cam () {
  if [[ $# -ne 1 ]]; then
    echo "Usage: cam id"
  else
    v4l2-ctl -d /dev/video"$1" --list-formats-ext
  fi
}

function flash-image () {
  if [[ $# -ne 1 ]]; then
    echo "Usage: cam id"
  else
    sudo dd if="$1" of=${2-"/dev/mmcblk0"} bs=1M status=progress 
  fi
}

function msh () {
  if [[ $# -ne 1 ]]; then
    echo "Usage: ${0} user"
    echo "Use to log in to a Coral device over USB."
  else
    ssh-keygen -f "/home/${USER}/.ssh/known_hosts" -R "192.168.100.2"
    ssh "${1}"@192.168.100.2
  fi
}

function jsh () {
  if [[ $# -ne 1 ]]; then
    echo "Usage: ${0} user"
    echo "Use to log in to a Jetson device over USB."
  else
    ssh-keygen -f "/home/${USER}/.ssh/known_hosts" -R "192.168.55.1"
    ssh "${1}"@192.168.55.1
  fi
}
