# Administration
alias up="sudo apt update && sudo apt upgrade --allow-downgrades -y"

function test_truecolor() {
  awk 'BEGIN{
      s="/\\/\\/\\/\\/\\"; s=s s s s s s s s;
      for (colnum = 0; colnum<77; colnum++) {
          r = 255-(colnum*255/76);
          g = (colnum*510/76);
          b = (colnum*255/76);
          if (g>255) g = 510-g;
          printf "\033[48;2;%d;%d;%dm", r,g,b;
          printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
          printf "%s\033[0m", substr(s,colnum+1,1);
      }
      printf "\n";
  }'
}

# SSH
alias ssa="ssh -A"
alias ssx="ssh -X"

# Systemd
alias start="sudo systemctl start"
alias stop="sudo systemctl stop"
alias restart="sudo systemctl restart"
alias status="sudo systemctl status"
alias sreload="sudo systemctl daemon-reload"

# Vifm
alias -g vfr="vifmrun"

# Nvim
alias -g nv="nvim"
alias -g vi="nvim"

function update_neovim() {
  wget -P /tmp https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
  chmod +x /tmp/nvim.appimage
  sudo mv /tmp/nvim.appimage /usr/local/bin/nvim
}

function list-packages() {
  dpkg-query -W --showformat='${Installed-Size;10}\t${Package}\n' | sort -k1,1n
}

function check-sha256() {
  if [[ $# -ne 2 ]]; then
    echo "Usage: ${0} <checksum> <file>"
  else
    echo "${1} ${2}" | shasum -a 256 --check
  fi
}

function check-md5() {
  if [[ $# -ne 2 ]]; then
    echo "Usage: ${0} <checksum> <file>"
  else
    echo "${1} ${2}" | md5sum --check
  fi
}
