# Stop all docker containers
function dsa() {
  docker container ls | awk 'NR>1 {print $1}' | xargs docker stop
}

# Removes all docker images labeled as <none>
function drmn() {
  docker images | grep none | awk '{print $3}' | xargs docker rmi
}

function drmai() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: ${0} repo"
    echo "Removes every image for the provided repo."
  else
    docker images | grep ${1} | awk '{print $3}' | xargs docker rmi
  fi
}

function dbuild (){
  DOCKER_BUILDKIT=1 docker build --ssh default "$@"
}

alias get-docker='curl -fsSL https://get.docker.com | sh'
