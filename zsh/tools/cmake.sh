alias mk="make -j$(nproc)"
alias cmb="sudo cmake --build ."
alias cmi="sudo cmake --install ."

function cm() {
	if [ -f ../CMakeLists.txt ]; then
		cmake .. -DCMAKE_EXPORT_COMPILE_COMMANDS=1 "$@"
	else
		compiledb make
	fi
}

function cmd() {
	if [ -f ../CMakeLists.txt ]; then
		cmake .. -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
			-DCMAKE_BUILD_TYPE=Debug "$@"
	else
		compiledb make
	fi
}

function cmr() {
	if [ -f ../CMakeLists.txt ]; then
		cmake .. -DCMAKE_BUILD_TYPE=Release "$@"
	else
		echo "No CMakeLists file found."
	fi
}
