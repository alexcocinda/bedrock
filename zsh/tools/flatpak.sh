alias gimp="/usr/bin/flatpak run \
  --branch=stable \
  --arch=x86_64 \
  --command=gimp \
  --filesystem=home \
  --file-forwarding \
  org.gimp.GIMP"

alias inkscape="/usr/bin/flatpak run \
  --branch=stable \
  --arch=x86_64 \
  --command=inkscape \
  --filesystem=home \
  --file-forwarding \
  org.inkscape.Inkscape"

alias glade="/usr/bin/flatpak run \
  --branch=stable \
  --arch=x86_64 \
  --command=glade \
  --filesystem=home \
  --file-forwarding \
  org.gnome.Glade"

alias devhelp="/usr/bin/flatpak run \
  --branch=stable \
  --arch=x86_64 \
  --command=devhelp \
  org.gnome.Devhelp"
