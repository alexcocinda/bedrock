function run_redis() {
  if [[ $# -lt 1 ]]; then
    echo "Usage: $(basename $0) <redis_tag>"
  else
    docker run -d --rm -it --net=host --name=redis \
      -v /opt/redis/data:/opt/redis/data \
      redis:"$1" \
      redis-server --save 60 1 --loglevel warning
  fi
}

alias stop_redis="docker container stop redis"
