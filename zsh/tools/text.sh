function search() {
  grep -rni --exclude-dir build "$1" .
}

function sedeasy() {
  sed -i "s/$(echo $1 | sed -e 's/\([[\/.*]\|\]\)/\\&/g')/$(echo $2 | sed -e 's/[\/&]/\\&/g')/g" $3
}

function replace() {
  grep -rl "$1" . | xargs sed -i 's/'"$1"'/'"$2"'/g'
}
