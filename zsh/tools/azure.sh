#!/bin/bash

alias storage-explorer="/opt/azure/storage-explorer/StorageExplorer"

function compute-device-key() {
  if [[ $# -ne 2 ]]; then
    echo "Usage: compute-device-key primary-key device-id"
  else
    az iot central device compute-device-key --primary-key "$1" --device-id "$2"
  fi
}

function compute-storage-key() {
  if [[ $# -ne 4 ]]; then
    echo "Usage: compute-storage-key account-name container-name permissions"
  else
    az storage container generate-sas \
      --account-key "$1" \
      --account-name "$2" \
      --name "$3" \
      --permissions "$4"
  fi
}

function download-data() {
  if [[ $# -lt 1 ]]; then
    echo "Usage: download-data <deployment-location> [<pattern>]"
  else
    az storage blob download-batch \
      -d . --pattern "${2-*}" \
      -s "$1" --account-name cradledata \
      --account-key ${AZURE_STORAGE_ACCOUNT_KEY}
  fi
}

function delete-data() {
  if [[ $# -lt 1 ]]; then
    echo "Usage: delete-data <deployment-location> [<pattern>]"
  else
    az storage blob delete-batch \
      --pattern "${2-*}" \
      -s "$1" --account-name cradledata \
      --account-key ${AZURE_STORAGE_ACCOUNT_KEY}
  fi
}
