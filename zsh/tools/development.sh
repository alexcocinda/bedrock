function mkx() {
  chmod +x "$1"
}

function uninstall() {
  if [[ "${1}" != "" ]]; then
      sudo rm -r /usr/local/include/"$1"
      sudo rm -r /usr/local/lib/"$1"
    else
      echo "Uninstall what?"
  fi
}

function clean() {
  current_directory=$(basename $(pwd))
  if [[ "debug release build" =~ "${current_directory}" ]]; then
    echo "Cleaning build artifacts ..."
    rm -rf *
  else
    echo "Not inside a build directory!"
  fi
}
