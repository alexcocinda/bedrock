# Wireguard

function wg-genkey() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: wg-genkey <id>"
  else
    wg genkey | tee ${1}_private_key | wg pubkey > ${1}_public_key
  fi
}
