function topic-create() {
  if [[ $# -lt 1 ]]; then
    echo "Usage: $(basename $0) <topic-name> <partitions-number> <replication-factor>"
  else
    kafka-topics.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} --topic $1 --create --partitions ${2-1} --replication-factor ${3-1};
  fi
}

function topics() {
  kafka-topics.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} --list
}

function topic-describe() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: $(basename $0) <topic-name>"
  else
    kafka-topics.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} --topic $1 --describe
  fi
}

function topic-delete() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: $(basename $0) <topic-name>"
  else
    kafka-topics.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} --topic $1 --delete
  fi
}

function produce-from-console() {
  if [[ $# -lt 1 ]]; then
    echo "Usage: $(basename $0) <topic-name> <property>"
  else
    kafka-console-producer.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} \
      --topic $1 --producer-property ${2-"acks=0"}
  fi
}

function consume-from-console() {
  if [[ $# -lt 1 ]]; then
    echo "Usage: $(basename $0) <topic-name>"
  else
    kafka-console-consumer.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} \
      --topic $1 ${2+"--from-beginning"}
  fi
}

function group-describe() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: $(basename $0) <topic-name>"
  else
    kafka-consumer-groups.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} --group $1 --describe
  fi
}

function groups() {
  kafka-consumer-groups.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} --list
}

function group-reset() {
  if [ -z ${2+x} ]; then
    kafka-consumer-groups.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} \
      --group $1 --all-topics --reset-offsets --to-earliest --execute
  else
    kafka-consumer-groups.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} \
      --group $1 --topic $2 --reset-offsets ${3-"--to-earliest"} --execute
  fi
}

function group-shift() {
  if [ -z ${2+x} ]; then
    kafka-consumer-groups.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} \
      --group $1 --reset-offsets --shift-by $2 --execute --all-topics
  else
    kafka-consumer-groups.sh --bootstrap-server ${BOOTSTRAP_SERVERS-"localhost:9092"} \
      --group $1 --reset-offsets --shift-by $2 --execute --topic $3
  fi
}

function run_kafka() {
  if [[ $# -lt 1 ]]; then
    echo "Usage: $(basename $0) <kafka_tag>"
  else
    docker run -d --rm --net=host --name=kafka \
      -e KAFKA_CFG_NODE_ID=0 \
      -e KAFKA_CFG_PROCESS_ROLES=controller,broker \
      -e KAFKA_CFG_LISTENERS=PLAINTEXT://:9092,CONTROLLER://:9093 \
      -e KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP=CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT \
      -e KAFKA_CFG_CONTROLLER_QUORUM_VOTERS=0@localhost:9093 \
      -e KAFKA_CFG_CONTROLLER_LISTENER_NAMES=CONTROLLER \
      -v /opt/bitnami/kafka/logs:/opt/bitnami/kafka/logs \
      bitnami/kafka:"$1"
  fi
}

alias stop_kafka="docker container stop kafka"
