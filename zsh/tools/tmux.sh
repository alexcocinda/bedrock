# Kills tmux server.
function tenplm() {
  tmux kill-server
}

function tshared() {
    tmux -S /tmp/shareds attach -t $1
}
