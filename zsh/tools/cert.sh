# Certificates

function generate-ca () {
  if [[ $# -ne 3 ]]; then
    echo "Usage: generate-ca <CN> <ca_key.pem> <ca_cert.pem>"
  else
    ipsec pki --gen --outform pem > "$2"
    ipsec pki --self --in "${2}" --dn "CN=$1" --ca --outform pem > "$3"
    openssl x509 -in "${3}" -outform der | base64 -w0 ; echo
  fi
}

function generate-client-cert () {
  if [[ $# -ne 4 ]]; then
    echo "Usage: generate-client-cert <client_id> <client_password> <ca_key.pem> <ca_cert.pem>"
  else
    ipsec pki --gen --outform pem > "${1}_key.pem"
    ipsec pki --pub --in "${1}_key.pem" | ipsec pki --issue --cacert ${4} --cakey ${3} --dn "CN=${1}" --san "${1}" --flag clientAuth --outform pem > "${1}_cert.pem"
    openssl pkcs12 -in "${1}_cert.pem" -inkey "${1}_key.pem" -certfile ${4} -export -out "${1}.p12" -password "pass:${2}"
    openssl pkcs12 -in "${1}.p12" -nodes -out "${1}_profileinfo.txt"
  fi
}

function generate-profile-info () {
  if [[ $# -ne 2 ]]; then
    echo "Usage: generate-profile-info <cert.p12> <out.txt>"
  else
    openssl pkcs12 -in "${1}" -nodes -out "${2}"
  fi
}
