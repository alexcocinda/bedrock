# System info
if [[ -z "${TMUX}" ]] || [[ $(tmux list-panes | wc -l) -eq 1 ]]; then neofetch; fi

# Prompt
# See https://github.com/sindresorhus/pure.
ZSH_THEME=""
fpath+=${HOME}/.zsh/pure
autoload -U promptinit; promptinit
prompt pure

# Set paths right
export PATH=/usr/local/bin:/sbin:${PATH}
export PATH=${HOME}/.local/bin:${PATH}
export PATH=${HOME}/.local/bin/scripts:${PATH}
export PATH=${HOME}/.config/vifm/plugins/vifmimg:/usr/local/bin:${PATH}
export MANPATH=${HOME}/.local/man:${MANPATH}

# Path to your oh-my-zsh installation.
export ZSH="${HOME}/.oh-my-zsh"

# History
# export SAVEHIST=5000
# export HISTSIZE=2000
# setopt EXTENDED_HISTORY
# setopt HIST_EXPIRE_DUPS_FIRST
# setopt HIST_FIND_NO_DUPS
# setopt HIST_REDUCE_BLANKS
# export HISTFILE="~/.cache/zsh/plm"

# Zsh options
setopt GLOB_COMPLETE
setopt SHARE_HISTORY
setopt APPEND_HISTORY
setopt INC_APPEND_HISTORY

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=3

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

plugins=(
  git
  docker
  docker-compose)

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export EDITOR='nvim'
else
  export EDITOR='nvim'
fi

# Vi mode
set -o vi

# Bindings
bindkey -s '^_' 'tmux^M'
bindkey -s '^E' 'clear^M'
bindkey -s '^F' 'vifmrun .^M'
bindkey -s '^G' 'lazygit^M'
bindkey -s '^V' 'nvim^M'

# Functions
for file in ~/.config/bedrock/zsh/tools/*
do
  source ${file}
done

# Completion
for file in ~/.config/bedrock/zsh/completion/*
do
  source ${file}
done

[[ "$(ls -A ~/.local/completion)" ]] && for file in ~/.local/completion/*
do
  source ${file}
done

# Configure fzf
[[ -f ~/.fzf.zsh ]] && source ~/.fzf.zsh
